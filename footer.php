<footer class="site-footer"><script language="javascript" type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script><script src="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script><script src="<?= get_stylesheet_directory_uri(); ?>/dist/js/app.js"></script><script>var words = [ <?php if (have_posts()) : while (have_posts()) : the_post(); ?> <?php

          // Check rows exists.
          if (have_rows('palavras')) :

            // Loop through rows.
            while (have_rows('palavras')) : the_row();

              $palavra = get_sub_field('palavra');

          ?> `<?= $palavra ?>`, <?php
            endwhile;


          endif;

        endwhile;

      endif; ?> ];


    function CheckMobile() {
      if (window.innerWidth >= 800) {
        return ({
          height: 14,
          width: 14
        })
      }
    }


    // start a word find game
    var gamePuzzle = wordfindgame.create(words, '#puzzle', '#words', CheckMobile());


    $('#solve').click(function() {
      wordfindgame.solve(gamePuzzle, words);
    });

    // // create just a puzzle, without filling in the blanks and print to console
    // var puzzle = wordfind.newPuzzle(words, {
    //   height: 50,
    //   width: 50
    // });
    // wordfind.print(puzzle);</script></footer>